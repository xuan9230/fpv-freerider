<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','LandingController@index');

Route::get('/download','UserController@download');
Route::get('/index','UserController@index');
Route::get('/scoreboard','UserController@scoreboard');

Route::get('/newuser','UserController@create');
Route::post('/waitMaskReg','ServiceController@store');
Route::post('/waitMaskLogin','ServiceController@login');

Route::get('register-form', function()
{
    return view('widgets.register-form');
});
