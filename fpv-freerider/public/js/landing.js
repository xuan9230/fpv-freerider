var landApp = angular.module('landing', ['ui.bootstrap']);

landApp.controller('formController', function ($scope) {
    $scope.showModal = false;
    $scope.toggleModal = function(){
        $scope.showModal = !$scope.showModal;
    };

    $('.main-nav ul').onePageNav({
      currentClass: 'active',
        changeHash: false,
        scrollSpeed: 900,
        scrollOffset: 0,
        scrollThreshold: 0.3,
        filter: ':not(.no-scroll)'
    });

    function menuToggle()
    {
      var windowWidth = $(window).width();

      if(windowWidth > 767 ){
        $(window).on('scroll', function(){
          if( $(window).scrollTop()>405 ){
            $('.main-nav').addClass('fixed-menu animated slideInDown');
          } else {
            $('.main-nav').removeClass('fixed-menu animated slideInDown');
          }
        });
      }else{
        
        $('.main-nav').addClass('fixed-menu animated slideInDown');
          
      }
    }

    menuToggle();

    $( window ).resize(function() {
      menuToggle();
    });
    
  });

landApp.directive('modal', function () {
    return {
      templateUrl:'register-form',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });










