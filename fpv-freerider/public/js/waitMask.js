var maskapp = angular.module('waitMask', []);

maskapp.controller('maskController', ['$scope',function ($scope) {
	$scope.nUsername = document.getElementById('d1').value;
    $scope.nPassword = document.getElementById('d2').value;   
    $scope.nEmail = document.getElementById('d3').value;  

    $scope.register = function() {
    	$scope.user = new Parse.User();
    	$scope.user.set("licensed", false);
		$scope.user.set("username", $scope.nUsername);
		$scope.user.set("password", $scope.nPassword);
		$scope.user.set("email", $scope.nEmail);

	  
		$scope.user.signUp(null, {
		  success: function(user) {
		    // Hooray! Let them use the app now.
		    document.location.href='/newuser';
		  },
		  error: function(user, error) {
		    // Show the error message somewhere and let the user try again.
		    alert("Error: " + error.code + " " + error.message);
		    document.location.href='/';
		  }
		});
    };

    $scope.login = function() {
    	Parse.User.logIn($scope.nUsername, $scope.nPassword, {
		  success: function(user) {
		    // Do stuff after successful login.
		    document.location.href='/index';
		  },
		  error: function(user, error) {
		    // The login failed. Check error to see why.
		    alert(error.message);
		    document.location.href='/';
		  }
		});
    };

    if($scope.nEmail=="") {
    	$scope.login();
    }
    else {
    	$scope.register();
    }
    

}]);