<!DOCTYPE html>
<html lang="en" ng-app="landing">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FPV Freerider</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">	
	<link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
</head><!--/head-->

<body>
	<header id="header" role="banner">		
		<div class="main-nav" aaa>
			<div class="container">
				<div class="header-top">
					<div class="pull-right social-icons">
						<a href="#"><i class="fa fa-tumblr"></i></a>
						<a href="https://www.facebook.com/fpvfreerider/"><i class="fa fa-facebook"></i></a>
					</div>
				</div>     
		        <div class="row">	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                <a class="navbar-brand" href="index.html">
		                	<img class="img-responsive" src="images/logo.png" alt="logo">
		                </a>                    
		            </div>
		            <div class="collapse navbar-collapse">
		                <ul class="nav navbar-nav navbar-right">                 
		                    <li class="scroll active"><a href="#home">Home</a></li>
		                    <li class="scroll"><a href="#feature1">Features</a></li>                     
		                    <li class="no-scroll"><a href="#tumblr">Voices</a></li>
		                    <li class="scroll"><a href="#download">Download</a></li>       
		                </ul>
		            </div>
		        </div>
	        </div>
        </div>                    
    </header>
    <!--/#header--> 

    <section id="home">	
		<div id="main-slider" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#main-slider" data-slide-to="0" class="active"></li>
				<li data-target="#main-slider" data-slide-to="1"></li>
			</ol>
			<div class="carousel-inner">
				<div class="item active">
					<img class="img-responsive" src="images/slider/bg1.jpg" alt="slider">						
					<div class="carousel-caption">
						<h2>FPV Drone Racing Simulator</h2>
						<h4>“If you were born without wings,<br/> 
						do nothing to prevent them from growing.”</h4>
						<a href="#download">DOWNLOAD NOW<i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="images/slider/bg2.jpg" alt="slider">	
					<div class="carousel-caption">
						<h2>Enjoy Views In The Sky </h2>
						<h4>“If you were born without wings,<br/> 
						do nothing to prevent them from growing.”</h4>
						<a href="#download">DOWNLOAD NOW<i class="fa fa-angle-right"></i></a>
					</div>
				</div>	
			</div>
		</div>    	
    </section>
	<!--/#home-->

	<section class="feature" id="feature1">
		<div class="feature_pic">				
			<img class="img-responsive" src="images/physical_engine.jpg">
		</div>
		<div class="about-content">					
			<h2>Realistic Physical Engine</h2>
			<p>We have created an extremely positive and relaxed environment all geared towards developing your skills whether you are an absolute beginner trying to get off the ground or an accomplished player looking to move to the next level. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
			<a href="#" class="btn btn-primary">View Details<i class="fa fa-angle-right"></i></a>		
		</div>
	</section>

	<section class="feature" id="feature2">
		<div class="about-content">					
			<h2>Multiplayer</h2>
			<p>We have created an extremely positive and relaxed environment all geared towards developing your skills whether you are an absolute beginner trying to get off the ground or an accomplished player looking to move to the next level. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
			<a href="#" class="btn btn-primary">View Details<i class="fa fa-angle-right"></i></a>		
		</div>
		<div class="feature_pic">				
			<img class="img-responsive" src="images/multi_player.jpg">
		</div>
	</section>

	<section class="feature" id="feature3">
		<div class="feature_pic">				
			<img class="img-responsive" src="images/multi_controller.png">
		</div>
		<div class="about-content">					
			<h2>Various Controllers Supported</h2>
			<p>We have created an extremely positive and relaxed environment all geared towards developing your skills whether you are an absolute beginner trying to get off the ground or an accomplished player looking to move to the next level. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
			<a href="#" class="btn btn-primary">View Details<i class="fa fa-angle-right"></i></a>		
		</div>
	</section>

	<section class="feature" id="feature4">
		<div class="about-content">					
			<h2>Affordable</h2>
			<p>We have created an extremely positive and relaxed environment all geared towards developing your skills whether you are an absolute beginner trying to get off the ground or an accomplished player looking to move to the next level. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
			<a href="#" class="btn btn-primary">View Details<i class="fa fa-angle-right"></i></a>		
		</div>
		<div class="feature_pic">				
			<img class="img-responsive" src="images/affordable.jpg">
		</div>
	</section>


	<!--/#feature-->
	
	<section id="tumblr">
		<div id="tumblr-feed" class="carousel slide" data-interval="false">
			<div class="twit">
				<img class="img-responsive" src="images/tumblr.png" alt="twit">
			</div>
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="text-center carousel-inner center-block">
						<div class="item active">
							<img src="images/tumblr/tumblr1.png" alt="">
							<p>我是傻逼 </p>
							<a href="#">http://t.co/yY7s1IfrAb 2 days ago</a>
						</div>
						<div class="item">
							<img src="images/tumblr/tumblr2.png" alt="">
							<p>我也是</p>
							<a href="#">http://t.co/yY7s1IfrAb 2 days ago</a>
						</div>
						<div class="item">
							<img src="images/tumblr/tumblr3.png" alt="">
							<p>我不服</p>
							<a href="#">http://t.co/yY7s1IfrAb 2 days ago</a>
						</div>
					</div>
<!-- 					<a class="tumblr-control-left" href="#tumblr-feed" data-slide="prev"><i class="fa fa-angle-left"></i></a>
					<a class="tumblr-control-right" href="#tumblr-feed" data-slide="next"><i class="fa fa-angle-right"></i></a>   -->   
					<!-- problems with directives -->
				</div>
			</div>
		</div>		
	</section>
	<!--/#tumblr-feed-->
	
<!-- 	<section id="sponsor">
		<div id="sponsor-carousel" class="carousel slide" data-interval="false">
			<div class="container">
				<div class="row">
					<div class="col-sm-10">
						<h2>Sponsors</h2>			
						<a class="sponsor-control-left" href="#sponsor-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
						<a class="sponsor-control-right" href="#sponsor-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
						<div class="carousel-inner">
							<div class="item active">
								<ul>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor1.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor2.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor3.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor4.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor5.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor6.png" alt=""></a></li>
								</ul>
							</div>
							<div class="item">
								<ul>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor6.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor5.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor4.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor3.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor2.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor1.png" alt=""></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>				
			</div>
			<div class="light">
				<img class="img-responsive" src="images/light.png" alt="">
			</div>
		</div>
	</section> -->
	<!--/#sponsor-->

	<section id="download">
		<div class="container">
			<div class="row">
				<h2 class="heading">All Platforms to Play With</h2>
				<div class="col-sm-3">
					<span class="fa fa-windows fa-5x"></span>
				</div>
				<div class="col-sm-3">
					<span class="fa fa-apple fa-5x"></span>
				</div>
				<div class="col-sm-3">
					<span class="fa fa-linux fa-5x"></span>
				</div>
				<div class="col-sm-3">
					<span class="fa fa-android fa-5x"></span>
				</div>
			</div>
		</div>

		{!! Form::open() !!}
   		{!! Form::close() !!}

		<div ng-controller="formController" class="container">
		  <button ng-click="toggleModal()" class="btn btn-default">Download</button>
		  <modal title="Become A Member To Download!" visible="showModal">
		      <uib-tabset>
			    <uib-tab heading="Login">
					{!! Form::open(['url'=>'/waitMaskLogin']) !!}
				    	<div class="form-group">
					       {!! Form::label('username','Username') !!}
					       {!! Form::text('username',null,['class'=>'form-control','placeholder'=>'Enter username']) !!}
					    </div>
					    <div class="form-group">
					       {!! Form::label('password','Password') !!}
					       {!! Form::password('password',['class'=>'form-control','placeholder'=>'Enter password']) !!}
					    </div>
					    <div class="form-group">
					       {!! Form::submit('Login',['class'=>'btn btn-success form-control','id'=>'regbtn','ng-click'=>'login()']) !!}
					   </div>
   					{!! Form::close() !!}
			    </uib-tab>
			    <uib-tab heading="Register">
					{!! Form::open(['url'=>'/waitMaskReg']) !!}
				    	<div class="form-group">
					       {!! Form::label('username','Username') !!}
					       {!! Form::text('username',null,['class'=>'form-control','placeholder'=>'Enter username']) !!}
					    </div>
				    	<div class="form-group">
					       {!! Form::label('email','Email address') !!}
					       {!! Form::email('email',null,['class'=>'form-control','placeholder'=>'Enter email']) !!}
					    </div>
					    <div class="form-group">
					       {!! Form::label('password','Password') !!}
					       {!! Form::password('password',['class'=>'form-control','placeholder'=>'Enter password']) !!}
					    </div>
					    <div class="form-group">
					       {!! Form::label('rppassword','Repeat Password') !!}
					       {!! Form::password('rppassword',['class'=>'form-control','placeholder'=>'Repeat password']) !!}
					    </div>
					    <div class="form-group">
					       {!! Form::submit('Register',['class'=>'btn btn-success form-control','id'=>'regbtn']) !!}
					   </div>	
   					{!! Form::close() !!}
			    </uib-tab>
			  </uib-tabset>
		  </modal>
		</div>

	</section>

	<!--/#download-->

    <footer id="footer">
        <div class="container">
            <div class="text-center">
                <p> Copyright  &copy;2015<a target="_blank" href="#"> FPV Freerider </a>. All Rights Reserved.</p>                
            </div>
        </div>
    </footer>
    <!--/#footer-->

    <!-- bower js -->
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/angular/angular.js"></script>
    <script src="/bower_components/angular-route/angular-route.min.js"></script>

    <script type="text/javascript" src="/js/ui-bootstrap-tpls-0.14.3.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/smoothscroll.js"></script>
    <script type="text/javascript" src="/js/jquery.parallax.js"></script>
    <script type="text/javascript" src="/js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="/js/jquery.nav.js"></script>
    <script type="text/javascript" src="/js/landing.js"></script>  
</body>
</html>