<div class="modal fade"> 
  <div class="modal-dialog">
    <div class="modal-content">
     <div class="modal-header">
       <h4 class="modal-title">{{ title }}</h4>
      </div>
     <div class="modal-body" ng-transclude></div> 
    </div>
 </div>
</div>
